#!/bin/bash

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

alias update='sudo apt update && sudo apt -y upgrade'
alias bashes='echo $SHLVL'

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias pgrep='pgrep -lai'

alias mv='mv -iv'
alias cp='cp -iv'
alias rm='rm -iv'

alias ls='ls -h --color=auto --group-directories-first'
alias ll='ls -l'
alias la='ls -a'
alias dir='ls -l'
alias d='dir'
alias dd='dir -a'
alias tree='tree -p -h --du --dirsfirst'
alias t='tree'
alias tt='tree -a'

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

alias ..='cd ..'
alias ...='cd ../../'
alias ....='cd ../../../'
alias .....='cd ../../../../'
alias ......='cd ../../../../../'
alias .......='cd ../../../../../../'
alias ........='cd ../../../../../../../'

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

alias gadd='git add .'
alias gcommit='git commit -a'
alias gpush='git push'
alias gpull='git pull'
# alias gclean='git clean -fd'
# alias greset='git reset --hard'
# alias gclone='git clone --depth=1'
alias gstatus='git status -vvv'

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

alias c='clear'
alias du='du -h'
alias less='less -r'
alias inet='ip a | grep -o "inet[^6].*" | cut -d" " -f2'
alias uniqu='awk '"'"'!x[$0]++'"'"
alias xclip='xclip -sel clip'

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

alias myhttp='python3 -m http.server'
alias myftp='python3 -m pyftpdlib'
