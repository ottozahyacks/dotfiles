#!/bin/bash

mkcd() {
    if [ $# -eq 1 ]; then
        mkdir -p $1 && cd $1
    fi
}

mktmp() {
    if [ $# -eq 0 ]; then
        cd $(mktemp -d)
    else
        mkdir -p /tmp/$1 && cd /tmp/$1
    fi
}
