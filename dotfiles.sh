#!/bin/bash
set -e

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

export DOTFILES="$(dirname $(readlink -f $0))"

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# Add custom bin folder to PATH env
[[ ":$PATH:" != *":${DOTFILES}/bin:"* ]] && export PATH="${DOTFILES}/bin:${PATH}"

# Add custom environment variables
source "${DOTFILES}/shell/shell-variables.sh"

# Add custom shell scripts (built in functions)
source "${DOTFILES}/shell/shell-functions.sh"

# Add custom aliases
source "${DOTFILES}/shell/shell-aliases.sh"

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

set +e
